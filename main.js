"use strict";

var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (Object.prototype.hasOwnProperty.call(b, p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        if (typeof b !== "function" && b !== null)
            throw new TypeError("Class extends value " + String(b) + " is not a constructor or null");
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
Object.defineProperty(exports, "__esModule", { value: true });
// -----------------------------------
// Variable declaration
var text = 'king of pirates';
console.log(text);
var x = 10;
var y = 20;
var sum;
var title = 'One piece';
var isBeginner = true;
var total = 0;
var name = 'Luffy';
var sentence = "My name is ".concat(name, "\nI am a beginner in Typescript");
console.log(sentence);
var n = null;
var u = undefined;
// let isNew: boolean = null
// let myName: string = undefined
var isNew = null;
var myName = undefined;
var list1 = [1, 2, 3];
var list2 = [1, 2, 3];
var person1 = ['Zoro', 25];
var anytypeOfarray = [1, 'Nami', 'zoro', 'sanji'];
console.log(person1);
console.log(anytypeOfarray);
var Color;
(function (Color) {
    Color[Color["Red"] = 5] = "Red";
    Color[Color["Green"] = 6] = "Green";
    Color[Color["Blue"] = 7] = "Blue";
})(Color || (Color = {}));
var c = Color.Green;
console.log(c);
// ----------------------------
// VaraibleTypes
var randomValue = 10;
randomValue = 'Luffy';
randomValue = true;
console.log(randomValue);
var myVariable = 10;
function hasName(obj) {
    return !!obj && typeof obj === 'object' && 'name' in obj;
}
if (hasName(myVariable)) {
    console.log(myVariable.name);
}
if (typeof myVariable === 'string') {
    console.log(myVariable.toUpperCase());
}
var a; //not set its type at initial stage
a = 10;
a = true;
var b = 20;
// b=true // not applicable because it was sets as number at initial stage
var multiType;
multiType = 20;
multiType = true;
// intelliSense support will not be there with anyType
var anyType;
anyType = 20;
anyType = true;
// Functions
function add(num1, num2) {
    return num1 + num2;
}
add(1, 2);
// add(2)
// add('a',2) // IntelliSense show's as error
function diff(num1, num2) {
    if (num2) {
        return num1 - num2;
    }
    else {
        return num1;
    }
}
diff(1, 2);
diff(5);
function addition(num1, num2) {
    if (num2 === void 0) { num2 = 10; }
    return num1 + num2;
}
addition(1, 2);
addition(2); //not showing error because of default value
function fullname(person) {
    console.log("".concat(person.firstname, " ").concat(person.lastName));
}
var p = {
    firstname: 'Bruce',
    lastName: 'hemanth'
};
fullname(p);
// ------------------------------
// classes
var Employee = /** @class */ (function () {
    function Employee(name) {
        this.employeeName = name;
    }
    Employee.prototype.greet = function () {
        console.log("Good Morning ".concat(this.employeeName));
    };
    return Employee;
}());
var emp1 = new Employee('King of Hell');
console.log(emp1.employeeName);
emp1.greet();
// ............................
// Inheritance
var Manager = /** @class */ (function (_super) {
    __extends(Manager, _super);
    function Manager(managerName) {
        return _super.call(this, managerName) || this;
    }
    Manager.prototype.delegateWork = function () {
        console.log('Manager delegating tasks');
    };
    return Manager;
}(Employee));
var m1 = new Manager('Attack on titan');
m1.delegateWork();
m1.greet();
console.log(m1.employeeName);
