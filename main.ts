export {}
// -----------------------------------
// Variable declaration

let text = 'king of pirates'
console.log(text)

let x = 10
let y = 20

let sum: string
const title = 'One piece'

let isBeginner: boolean = true
let total: number = 0
let name: string = 'Luffy'

let sentence: string = `My name is ${name}
I am a beginner in Typescript`

console.log(sentence)

let n: null = null
let u: undefined = undefined

// let isNew: boolean = null
// let myName: string = undefined

let isNew: boolean | null = null
let myName: string | undefined = undefined

let list1: number[] = [1, 2, 3]
let list2: Array<number> = [1, 2, 3]

let person1: [string, number] = ['Zoro', 25]
let anytypeOfarray: any[] = [1, 'Nami', 'zoro', 'sanji']

console.log(person1)
console.log(anytypeOfarray)

enum Color {
  Red = 5,
  Green,
  Blue
}
let c: Color = Color.Green

console.log(c)

// ----------------------------
// VaraibleTypes
let randomValue: any = 10
randomValue = 'Luffy'
randomValue = true
console.log(randomValue)

let myVariable: unknown = 10

function hasName (obj: any): obj is { name: string } {
  return !!obj && typeof obj === 'object' && 'name' in obj
}

if (hasName(myVariable)) {
  console.log(myVariable.name)
}
if (typeof myVariable === 'string') {
  console.log(myVariable.toUpperCase())
}

let a //not set its type at initial stage
a = 10
a = true

let b = 20
// b=true // not applicable because it was sets as number at initial stage

let multiType: number | boolean
multiType = 20
multiType = true

// intelliSense support will not be there with anyType

let anyType: any
anyType = 20
anyType = true

// Functions
function add (num1: number, num2: number): number {
  return num1 + num2
}

add(1, 2)
// add(2)
// add('a',2) // IntelliSense show's as error

function diff (num1: number, num2?: number): number {
  if (num2) {
    return num1 - num2
  } else {
    return num1
  }
}

diff(1, 2)
diff(5)

function addition (num1: number, num2: number = 10): number {
  return num1 + num2
}

addition(1, 2)
addition(2) //not showing error because of default value

// ----------------------------------------
// interface

interface Person {
  firstname: string
  lastName?: string // optional
}

function fullname (person: Person) {
  console.log(`${person.firstname} ${person.lastName}`)
}

let p = {
  firstname: 'Bruce',
  lastName: 'hemanth'
}
fullname(p)

// ------------------------------
// classes

class Employee {
  employeeName: string

  constructor (name: string) {
    this.employeeName = name
  }

  greet () {
    console.log(`Good Morning ${this.employeeName}`)
  }
}

let emp1 = new Employee('King of Hell')
console.log(emp1.employeeName)
emp1.greet()

// ............................
// Inheritance

class Manager extends Employee {
  constructor (managerName: string) {
    super(managerName)
  }
  delegateWork () {
    console.log('Manager delegating tasks')
  }
}

let m1 = new Manager('Attack on titan')
m1.delegateWork()
m1.greet()
console.log(m1.employeeName)
